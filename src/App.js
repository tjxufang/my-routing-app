import React from 'react'
import {BrowserRouter, Link, Route, useHistory, useLocation, useParams} from "react-router-dom";
import Page6 from "./page6";

const Page1 = props => {
    console.log('props', props)
    return (
        <div>
            <h1>Page 1</h1>
            <img src="https://s7d2.scene7.com/is/image/HermanMillerStore/b2c_3x4crop?$image_src=HermanMillerStore/Lino_Chair_defaultfront&$b2c_907x680_jpeg$" width='100px' alt=""/>
        </div>
    )
}

class Page2 extends React.Component { // class component
    render() {
        return (
            <div>
                <h1>Page 2</h1>
            </div>
        )
    }
}

const Page3 = props => {
    const songId = props.match.params.id
    let page3 = useParams() // props.match.params {id: id}
    let history = useHistory()
    const jumpTo = (path, id) => {
        history.push(path, {productId: id})
    }
    return (
        <div>
            <h1>Page3, {`input songId is ${songId}`}</h1>
            <button onClick={() => jumpTo('/page4', songId + 'class40')}>jump to page4</button>
            <button onClick={() => history.goBack()}>back</button>
        </div>
    )
}

class Page4 extends React.Component {
    render() {
        return (
            <div>
                <h1>Page 4, {this.props.location.state.productId}</h1>
                <button onClick={() => this.props.history.push('/page5', {singer: 'singer1'})}>jump to page5</button>
            </div>
        )
    }
}

const Page5 = props => {
    console.log('page5 props', props)
    const location = useLocation()
    // let history = useHistory()
    return (
        <div>
            <h1>Page 5</h1>
            <p>{location.state.singer}</p>
            <button onClick={() => props.history.push('/page6')}>jump to page6</button>
        </div>
    )
}

const Page404 = () => {
    return (
        <div>
            <h1>404 page</h1>
        </div>
    )
}
const NavBar = () => {
    return (
        <div>
            <nav className="navbar navbar-dark bg-primary">
                <Link to="/" type="button" className="btn btn-primary">Home</Link>
                <Link to="/page2" type="button" className="btn btn-primary">page2</Link>
                <Link to="/page3/1" type="button" className="btn btn-primary">page3</Link>
            </nav>
        </div>
    )
}

function App() {
    return (
        <div>
            <BrowserRouter>
                <NavBar/>
                <div>
                    <Route path="/" exact component={Page1}/>
                    <Route path="/page2" exact component={Page2}/>
                    <Route path="/page3/:id" exact component={Page3}/>
                    <Route path="/page4" exact component={Page4}/>
                    <Route path="/page5" exact component={Page5}/>
                    <Route path="/page6" exact component={Page6}/>
                    <Route path="/404" exact component={Page404}/>
                </div>
            </BrowserRouter>
        </div>
    );
}

export default App;
